import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-paises',
  templateUrl: './lista-paises.page.html',
  styleUrls: ['./lista-paises.page.scss'],
})
export class ListaPaisesPage implements OnInit {

  private listaPaises = [
    {
      imageURL: '../assets/images/af.png',
      cancion: 'http://www.nationalanthems.info/af.mp3',
      orden: '1',
      nombreC: 'Afganistán',
      nombreOficial: 'La República Islámica del Afganistán',
      sigla2: 'AF',
      sigla3: 'AFG',
      codigo: '004'
    },
    {
      imageURL: '../assets/images/za.png',
      cancion: 'http://www.nationalanthems.info/za.mp3',
      orden: '2',
      nombreC: 'Unión Africana',
      nombreOficial: 'Unión Africana',
      sigla2: 'ZA',
      sigla3: 'ZAF',
      codigo: '710'
    },
    {
      imageURL: '../assets/images/al.png',
      cancion: 'http://www.nationalanthems.info/al.mp3',
      orden: '3',
      nombreC: 'Albania',
      nombreOficial: 'La República de Albania',
      sigla2: 'AL',
      sigla3: 'ALB',
      codigo: '008'
    },
    {
      imageURL: '../assets/images/de.png',
      cancion: 'http://www.nationalanthems.info/de.mp3',
      orden: '4',
      nombreC: 'Alemania',
      nombreOficial: 'La República Federal de Alemania',
      sigla2: 'DE',
      sigla3: 'DEU',
      codigo: '276'
    },
    {
      imageURL: '../assets/images/ad.png',
      cancion: 'http://www.nationalanthems.info/ad.mp3',
      orden: '5',
      nombreC: 'Andorra',
      nombreOficial: 'El Principado de Andorra',
      sigla2: 'AD',
      sigla3: 'AND',
      codigo: '020'
    },
    {
      imageURL: '../assets/images/ao.png',
      cancion: 'http://www.nationalanthems.info/ao.mp3',
      orden: '6',
      nombreC: 'Angola',
      nombreOficial: 'La República de Angola',
      sigla2: 'AO',
      sigla3: 'AGO',
      codigo: '024'
    },
    {
      imageURL: '../assets/images/ag.png',
      cancion: 'http://www.nationalanthems.info/ag.mp3',
      orden: '7',
      nombreC: 'Antigua y Barbuda',
      nombreOficial: 'Antigua y Barbuda',
      sigla2: 'AG',
      sigla3: 'ATG',
      codigo: '028'
    },
    {
      imageURL: '../assets/images/sa.png',
      cancion: 'http://www.nationalanthems.info/sa.mp3',
      orden: '8',
      nombreC: 'Arabia Saudita',
      nombreOficial: 'El Reino de la Arabia Saudita',
      sigla2: 'SA',
      sigla3: 'SAU',
      codigo: '682'
    },
    {
      imageURL: '../assets/images/al.png',
      cancion: 'http://www.nationalanthems.info/dz.mp3',
      orden: '9',
      nombreC: 'Argelia',
      nombreOficial: 'La República Argelina Democrática y Popular',
      sigla2: 'DZ',
      sigla3: 'DZA',
      codigo: '012'
    },
    {
      imageURL: '../assets/images/ar.png',
      cancion: 'http://www.nationalanthems.info/ar.mp3',
      orden: '10',
      nombreC: 'Argentina',
      nombreOficial: 'La República Argentina ',
      sigla2: 'AR',
      sigla3: 'ARG',
      codigo: '032'
    },
    {
      imageURL: '../assets/images/am.png',
      cancion: 'http://www.nationalanthems.info/am.mp3',
      orden: '11',
      nombreC: 'Armenia',
      nombreOficial: 'La República de Armenia',
      sigla2: 'AM',
      sigla3: 'ARM',
      codigo: '51'
    },
    {
      imageURL: '../assets/images/au.png',
      cancion: 'http://www.nationalanthems.info/au.mp3',
      orden: '12',
      nombreC: 'Australia',
      nombreOficial: 'Australia',
      sigla2: 'AU',
      sigla3: 'AUS',
      codigo: '036'
    },
    {
      imageURL: '../assets/images/at.png',
      cancion: 'http://www.nationalanthems.info/at.mp3',
      orden: '13',
      nombreC: 'Austria',
      nombreOficial: 'La República de Austria',
      sigla2: 'AT',
      sigla3: 'AUT',
      codigo: '040'
    },
    {
      imageURL: '../assets/images/az.png',
      cancion: 'http://www.nationalanthems.info/az.mp3',
      orden: '14',
      nombreC: 'Azerbaiyán',
      nombreOficial: 'La República de Azerbaiyán',
      sigla2: 'AZ ',
      sigla3: 'AZE',
      codigo: '31'
    },
    {
      imageURL: '../assets/images/bs.png',
      cancion: 'http://www.nationalanthems.info/bs.mp3',
      orden: '15',
      nombreC: 'Bahamas',
      nombreOficial: 'El Commonwealth de las Bahamas',
      sigla2: 'BS',
      sigla3: 'BHS',
      codigo: '044'
    },
    {
      imageURL: '../assets/images/bh.png',
      cancion: 'http://www.nationalanthems.info/bh.mp3',
      orden: '16',
      nombreC: 'Bahrein',
      nombreOficial: 'El Reino de Bahrein',
      sigla2: 'BH',
      sigla3: 'BHR',
      codigo: '048'
    },
    {
      imageURL: '../assets/images/bd.png',
      cancion: 'http://www.nationalanthems.info/bd.mp3',
      orden: '17',
      nombreC: 'Bangladesh',
      nombreOficial: 'La República Popular de Bangladesh',
      sigla2: 'BD',
      sigla3: 'BGD',
      codigo: '050'
    },
    {
      imageURL: '../assets/images/bb.png',
      cancion: 'http://www.nationalanthems.info/bb.mp3',
      orden: '18',
      nombreC: 'Barbados',
      nombreOficial: 'Barbados',
      sigla2: 'BB',
      sigla3: 'BRB',
      codigo: '052'
    },
    {
      imageURL: '../assets/images/by.png',
      cancion: 'http://www.nationalanthems.info/by.mp3',
      orden: '19',
      nombreC: 'Belarús',
      nombreOficial: 'La República de Belarús',
      sigla2: 'BY',
      sigla3: 'BLR',
      codigo: '112'
    },
    {
      imageURL: '../assets/images/be.png',
      cancion: 'http://www.nationalanthems.info/be.mp3',
      orden: '20',
      nombreC: 'Bélgica',
      nombreOficial: 'El Reino de Bélgica',
      sigla2: 'BE',
      sigla3: 'BEL',
      codigo: '056'
    },
    {
      imageURL: '../assets/images/bz.png',
      cancion: 'http://www.nationalanthems.info/bz.mp3',
      orden: '21',
      nombreC: 'Belice',
      nombreOficial: 'Belice',
      sigla2: 'BZ',
      sigla3: 'BLZ',
      codigo: '084'
    },
    {
      imageURL: '../assets/images/bj.png',
      cancion: 'http://www.nationalanthems.info/bj.mp3',
      orden: '22',
      nombreC: 'Benin',
      nombreOficial: 'La República de Benin',
      sigla2: 'BJ',
      sigla3: 'BEN',
      codigo: '204'
    },
    {
      imageURL: '../assets/images/bt.png',
      cancion: 'http://www.nationalanthems.info/bt.mp3',
      orden: '23',
      nombreC: 'Bhután',
      nombreOficial: 'El Reino de Bhután',
      sigla2: 'BT',
      sigla3: 'BTN',
      codigo: '064'
    },
    {
      imageURL: '../assets/images/bo.png',
      cancion: 'http://www.nationalanthems.info/bo.mp3',
      orden: '24',
      nombreC: 'Bolivia',
      nombreOficial: 'Estado Plurinacional de Bolivia',
      sigla2: 'BO',
      sigla3: 'BOL',
      codigo: '068'
    },
    {
      imageURL: '../assets/images/ba.png',
      cancion: 'http://www.nationalanthems.info/ba.mp3',
      orden: '25',
      nombreC: 'Bosnia y Herzegovina',
      nombreOficial: 'Bosnia y Herzegovina',
      sigla2: 'BA',
      sigla3: 'BIH',
      codigo: '070'
    },
    {
      imageURL: '../assets/images/bw.png',
      cancion: 'http://www.nationalanthems.info/bw.mp3',
      orden: '26',
      nombreC: 'Botswana',
      nombreOficial: 'La República de Botswana',
      sigla2: 'BW',
      sigla3: 'BWA',
      codigo: '072'
    },
    {
      imageURL: '../assets/images/br.png',
      cancion: 'http://www.nationalanthems.info/br.mp3',
      orden: '27',
      nombreC: 'Brasil',
      nombreOficial: 'La República Federativa del Brasil',
      sigla2: 'BR',
      sigla3: 'BRA',
      codigo: '076'
    },
    {
      imageURL: '../assets/images/bn.png',
      cancion: 'http://www.nationalanthems.info/bn.mp3',
      orden: '28',
      nombreC: 'Brunei Darussalam',
      nombreOficial: 'Brunei Darussalam',
      sigla2: 'BN',
      sigla3: 'BRN',
      codigo: '096'
    },
    {
      imageURL: '../assets/images/bg.png',
      cancion: 'http://www.nationalanthems.info/bg.mp3',
      orden: '29',
      nombreC: 'Bulgaria',
      nombreOficial: 'La República de Bulgaria',
      sigla2: 'BG',
      sigla3: 'BGR',
      codigo: '100'
    },
    {
      imageURL: '../assets/images/bf.png',
      cancion: 'http://www.nationalanthems.info/bf.mp3',
      orden: '30',
      nombreC: 'Burkina Faso',
      nombreOficial: 'Burkina Faso',
      sigla2: 'BF',
      sigla3: 'BFA',
      codigo: '854'
    },
    {
      imageURL: '../assets/images/bi.png',
      cancion: 'http://www.nationalanthems.info/bi.mp3',
      orden: '31',
      nombreC: 'Burundi',
      nombreOficial: 'La República de Burundi',
      sigla2: 'BI',
      sigla3: 'BDI',
      codigo: '108'
    },
    {
      imageURL: '../assets/images/cv.png',
      cancion: 'http://www.nationalanthems.info/cv.mp3',
      orden: '32',
      nombreC: 'Cabo Verde',
      nombreOficial: 'La República de Cabo Verde',
      sigla2: 'CV',
      sigla3: 'CPV',
      codigo: '132'
    },
    {
      imageURL: '../assets/images/cm.png',
      cancion: 'http://www.nationalanthems.info/cm.mp3',
      orden: '33',
      nombreC: 'Camerún',
      nombreOficial: 'La República del Camerún',
      sigla2: 'CM',
      sigla3: 'CMR',
      codigo: '120'
    },
    {
      imageURL: '../assets/images/kh.png',
      cancion: 'http://www.nationalanthems.info/kh.mp3',
      orden: '34',
      nombreC: 'Camboya',
      nombreOficial: 'La República de Camboya',
      sigla2: 'KH',
      sigla3: 'KHM',
      codigo: '116'
    },
    {
      imageURL: '../assets/images/ca.png',
      cancion: 'http://www.nationalanthems.info/ca.mp3',
      orden: '35',
      nombreC: 'Canadá',
      nombreOficial: 'Canadá',
      sigla2: 'CA',
      sigla3: 'CAN',
      codigo: '124'
    },
    {
      imageURL: '../assets/images/td.png',
      cancion: 'http://www.nationalanthems.info/td.mp3',
      orden: '36',
      nombreC: 'Chad',
      nombreOficial: 'La República del Chad',
      sigla2: 'TD',
      sigla3: 'TCD',
      codigo: '148'
    },
    {
      imageURL: '../assets/images/cz.png',
      cancion: 'http://www.nationalanthems.info/cz.mp3',
      orden: '37',
      nombreC: 'Chequia',
      nombreOficial: 'La República Checa',
      sigla2: 'CZ',
      sigla3: 'CZE',
      codigo: '203'
    },
    {
      imageURL: '../assets/images/cl.png',
      cancion: 'http://www.nationalanthems.info/cl.mp3',
      orden: '38',
      nombreC: 'Chile',
      nombreOficial: 'La República de Chile',
      sigla2: 'CL',
      sigla3: 'CHL',
      codigo: '152'
    },
    {
      imageURL: '../assets/images/cn.png',
      cancion: 'http://www.nationalanthems.info/cn.mp3',
      orden: '39',
      nombreC: 'China',
      nombreOficial: 'La República Popular China',
      sigla2: 'CN',
      sigla3: 'CHN',
      codigo: '156'
    },
    {
      imageURL: '../assets/images/cy.png',
      cancion: 'http://www.nationalanthems.info/gr.mp3',
      orden: '40',
      nombreC: 'Chipre',
      nombreOficial: 'La República de Chipre',
      sigla2: 'CY',
      sigla3: 'CYP',
      codigo: '196'
    },
    {
      imageURL: '../assets/images/co.png',
      cancion: 'http://www.nationalanthems.info/co.mp3',
      orden: '41',
      nombreC: 'Colombia',
      nombreOficial: 'La República de Colombia',
      sigla2: 'CO',
      sigla3: 'COL',
      codigo: '170'
    },
    {
      imageURL: '../assets/images/cg.png',
      cancion: 'http://www.nationalanthems.info/cg.mp3',
      orden: '42',
      nombreC: 'Congo',
      nombreOficial: 'La República del Congo',
      sigla2: 'CG',
      sigla3: 'COG',
      codigo: '178'
    },
    {
      imageURL: '../assets/images/cr.png',
      cancion: 'http://www.nationalanthems.info/cr.mp3',
      orden: '43',
      nombreC: 'Costa Rica',
      nombreOficial: 'La República de Costa Rica',
      sigla2: 'CR',
      sigla3: 'CRI',
      codigo: '188'
    },
    {
      imageURL: '../assets/images/hr.png',
      cancion: 'http://www.nationalanthems.info/hr.mp3',
      orden: '44',
      nombreC: 'Croacia',
      nombreOficial: 'La República de Croacia',
      sigla2: 'HR',
      sigla3: 'HRV',
      codigo: '191'
    },
    {
      imageURL: '../assets/images/cu.png',
      cancion: 'http://www.nationalanthems.info/cu.mp3',
      orden: '45',
      nombreC: 'Cuba',
      nombreOficial: 'La República de Cuba',
      sigla2: 'CU',
      sigla3: 'CUB',
      codigo: '192'
    },
    {
      imageURL: '../assets/images/ci.png',
      cancion: 'http://www.nationalanthems.info/ci.mp3',
      orden: '46',
      nombreC: 'Côte dIvoire',
      nombreOficial: 'La República de Côte dIvoire',
      sigla2: 'CI',
      sigla3: 'CIV',
      codigo: '384'
    },
    {
      imageURL: '../assets/images/km.png',
      cancion: 'http://www.nationalanthems.info/km.mp3',
      orden: '47',
      nombreC: 'Comoras',
      nombreOficial: 'La Unión de las Comoras',
      sigla2: 'KM',
      sigla3: 'COM',
      codigo: '174'
    },
    {
      imageURL: '../assets/images/dk.png',
      cancion: 'http://www.nationalanthems.info/dk.mp3',
      orden: '48',
      nombreC: 'Dinamarca',
      nombreOficial: 'El Reino de Dinamarca',
      sigla2: 'DK',
      sigla3: 'DNK',
      codigo: '208'
    },
    {
      imageURL: '../assets/images/dj.png',
      cancion: 'http://www.nationalanthems.info/dj.mp3',
      orden: '49',
      nombreC: 'Djibouti',
      nombreOficial: 'La República de Djibouti',
      sigla2: 'DJ',
      sigla3: 'DJI',
      codigo: '262'
    },
    {
      imageURL: '../assets/images/dm.png',
      cancion: 'http://www.nationalanthems.info/dm.mp3',
      orden: '50',
      nombreC: 'Dominica',
      nombreOficial: 'El Commonwealth de Dominica',
      sigla2: 'DM',
      sigla3: 'DMA',
      codigo: '212'
    },
    {
      imageURL: '../assets/images/eg.png',
      cancion: 'http://www.nationalanthems.info/eg.mp3',
      orden: '51',
      nombreC: 'Egipto',
      nombreOficial: 'La República Árabe de Egipto',
      sigla2: 'EG',
      sigla3: 'EGY',
      codigo: '818'
    },
    {
      imageURL: '../assets/images/sv.png',
      cancion: 'http://www.nationalanthems.info/sv.mp3',
      orden: '52',
      nombreC: 'El Salvador',
      nombreOficial: 'La República de El Salvador',
      sigla2: 'SV',
      sigla3: 'SLV',
      codigo: '222'
    },
    {
      imageURL: '../assets/images/ae.png',
      cancion: 'http://www.nationalanthems.info/ae.mp3',
      orden: '53',
      nombreC: 'Emiratos Árabes Unidos',
      nombreOficial: 'Los Emiratos Árabes Unidos',
      sigla2: 'AE',
      sigla3: 'ARE',
      codigo: '784'
    },
    {
      imageURL: '../assets/images/ec.png',
      cancion: 'http://www.nationalanthems.info/ec.mp3',
      orden: '54',
      nombreC: 'Ecuador',
      nombreOficial: 'La República del Ecuador',
      sigla2: 'EC',
      sigla3: 'ECU',
      codigo: '218'
    },
    {
      imageURL: '../assets/images/er.png',
      cancion: 'http://www.nationalanthems.info/er.mp3',
      orden: '55',
      nombreC: 'Eritrea',
      nombreOficial: 'El Estado de Eritrea',
      sigla2: 'ER',
      sigla3: 'ERI',
      codigo: '232'
    },
    {
      imageURL: '../assets/images/sk.png',
      cancion: 'http://www.nationalanthems.info/sk.mp3',
      orden: '56',
      nombreC: 'Eslovaquia',
      nombreOficial: 'La República Eslovaca',
      sigla2: 'SK',
      sigla3: 'SVK',
      codigo: '703'
    },
    {
      imageURL: '../assets/images/si.png',
      cancion: 'http://www.nationalanthems.info/si.mp3',
      orden: '57',
      nombreC: 'Eslovenia',
      nombreOficial: 'La República de Eslovenia',
      sigla2: 'SI',
      sigla3: 'SVN',
      codigo: '705'
    },
    {
      imageURL: '../assets/images/es.png',
      cancion: 'http://www.nationalanthems.info/es.mp3',
      orden: '58',
      nombreC: 'España',
      nombreOficial: 'El Reino de España',
      sigla2: 'ES',
      sigla3: 'ESP',
      codigo: '724'
    },
    {
      imageURL: '../assets/images/us.png',
      cancion: 'http://www.nationalanthems.info/us.mp3',
      orden: '59',
      nombreC: 'Estados Unidos de América',
      nombreOficial: 'Los Estados Unidos de América',
      sigla2: 'US',
      sigla3: 'USA',
      codigo: '840'
    },
    {
      imageURL: '../assets/images/ee.png',
      cancion: 'http://www.nationalanthems.info/ee.mp3',
      orden: '60',
      nombreC: 'Estonia',
      nombreOficial: 'La República de Estonia',
      sigla2: 'EE',
      sigla3: 'EST',
      codigo: '233'
    },
    {
      imageURL: '../assets/images/sz.png',
      cancion: 'http://www.nationalanthems.info/sz.mp3',
      orden: '61',
      nombreC: 'Eswatini',
      nombreOficial: 'El Reino de Eswatini',
      sigla2: 'SZ',
      sigla3: 'SWZ',
      codigo: '748'
    },
    {
      imageURL: '../assets/images/et.png',
      cancion: 'http://www.nationalanthems.info/et.mp3',
      orden: '62',
      nombreC: 'Etiopía',
      nombreOficial: 'La República Democrática Federal de Etiopía',
      sigla2: 'ET',
      sigla3: 'ETH',
      codigo: '231'
    },
    {
      imageURL: '../assets/images/fj.png',
      cancion: 'http://www.nationalanthems.info/fj.mp3',
      orden: '63',
      nombreC: 'Fiji',
      nombreOficial: 'La República de Fiji',
      sigla2: 'FJ',
      sigla3: 'FJI',
      codigo: '242'
    },
    {
      imageURL: '../assets/images/ph.png',
      cancion: 'http://www.nationalanthems.info/ph.mp3',
      orden: '64',
      nombreC: 'Filipinas',
      nombreOficial: 'La República de Filipinas',
      sigla2: 'PH',
      sigla3: 'PHL',
      codigo: '608'
    },
    {
      imageURL: '../assets/images/fi.png',
      cancion: 'http://www.nationalanthems.info/fi.mp3',
      orden: '65',
      nombreC: 'Finlandia',
      nombreOficial: 'La República de Finlandia',
      sigla2: 'FI',
      sigla3: 'FIN',
      codigo: '246'
    },
    {
      imageURL: '../assets/images/fr.png',
      cancion: 'http://www.nationalanthems.info/fr.mp3',
      orden: '66',
      nombreC: 'Francia',
      nombreOficial: 'La República Francesa',
      sigla2: 'FR',
      sigla3: 'FRA',
      codigo: '250'
    },
    {
      imageURL: '../assets/images/ga.png',
      cancion: 'http://www.nationalanthems.info/ga.mp3',
      orden: '67',
      nombreC: 'Gabón',
      nombreOficial: 'La República Gabonesa',
      sigla2: 'GA',
      sigla3: 'GAB',
      codigo: '266'
    },
    {
      imageURL: '../assets/images/gm.png',
      cancion: 'http://www.nationalanthems.info/gm.mp3',
      orden: '68',
      nombreC: 'Gambia',
      nombreOficial: 'La República de Gambia',
      sigla2: 'GM',
      sigla3: 'GMB',
      codigo: '270'
    },
    {
      imageURL: '../assets/images/gh.png',
      cancion: 'http://www.nationalanthems.info/gh.mp3',
      orden: '69',
      nombreC: 'Ghana',
      nombreOficial: 'La República de Ghana',
      sigla2: 'GH',
      sigla3: 'GHA',
      codigo: '288'
    },
    {
      imageURL: '../assets/images/ge.png',
      cancion: 'http://www.nationalanthems.info/ge.mp3',
      orden: '70',
      nombreC: 'Georgia',
      nombreOficial: 'Georgia',
      sigla2: 'GE',
      sigla3: 'GEO',
      codigo: '268'
    },
    {
      imageURL: '../assets/images/gb.png',
      cancion: 'http://www.nationalanthems.info/gb.mp3',
      orden: '71',
      nombreC: 'Gran Bretaña',
      nombreOficial: 'Reino Unido de Gran Bretaña',
      sigla2: 'GB',
      sigla3: 'GBR',
      codigo: '826'
    },
    {
      imageURL: '../assets/images/gd.png',
      cancion: 'http://www.nationalanthems.info/gd.mp3',
      orden: '72',
      nombreC: 'Granada',
      nombreOficial: 'Granada',
      sigla2: 'GD',
      sigla3: 'GRD',
      codigo: '308'
    },
    {
      imageURL: '../assets/images/gr.png',
      cancion: 'http://www.nationalanthems.info/gr.mp3',
      orden: '73',
      nombreC: 'Grecia',
      nombreOficial: 'La República Helénica',
      sigla2: 'GR',
      sigla3: 'GRC',
      codigo: '300'
    },
    {
      imageURL: '../assets/images/gl.png',
      cancion: 'http://www.nationalanthems.info/gl.mp3',
      orden: '74',
      nombreC: 'Groenlandia',
      nombreOficial: 'Groenlandia Kalaallit Nunaat',
      sigla2: 'GL',
      sigla3: 'GRL',
      codigo: '304'
    },
    {
      imageURL: '../assets/images/gt.png',
      cancion: 'http://www.nationalanthems.info/gt.mp3',
      orden: '75',
      nombreC: 'Guatemala',
      nombreOficial: 'La República de Guatemala',
      sigla2: 'GT',
      sigla3: 'GTM',
      codigo: '320'
    },
    {
      imageURL: '../assets/images/gy.png',
      cancion: 'http://www.nationalanthems.info/gy.mp3',
      orden: '76',
      nombreC: 'Guyana',
      nombreOficial: 'La República Cooperativa de Guyana',
      sigla2: 'GY',
      sigla3: 'GUY',
      codigo: '328'
    },
    {
      imageURL: '../assets/images/gn.png',
      cancion: 'http://www.nationalanthems.info/gn.mp3',
      orden: '77',
      nombreC: 'Guinea',
      nombreOficial: 'La República de Guinea',
      sigla2: 'GN',
      sigla3: 'GIN',
      codigo: '324'
    },
    {
      imageURL: '../assets/images/gq.png',
      cancion: 'http://www.nationalanthems.info/gq.mp3',
      orden: '78',
      nombreC: 'Guinea Ecuatorial',
      nombreOficial: 'La República de Guinea Ecuatorial',
      sigla2: 'GQ',
      sigla3: 'GNQ',
      codigo: '226'
    },
    {
      imageURL: '../assets/images/gw.png',
      cancion: 'http://www.nationalanthems.info/gw.mp3',
      orden: '79',
      nombreC: 'Guinea Bissau',
      nombreOficial: 'La República de Guinea-Bissau',
      sigla2: 'GW',
      sigla3: 'GNB',
      codigo: '624'
    },
    {
      imageURL: '../assets/images/ht.png',
      cancion: 'http://www.nationalanthems.info/ht.mp3',
      orden: '80',
      nombreC: 'Haití',
      nombreOficial: 'La República de Haití',
      sigla2: 'HT',
      sigla3: 'HTI',
      codigo: '332'
    },
    {
      imageURL: '../assets/images/hn.png',
      cancion: 'http://www.nationalanthems.info/hn.mp3',
      orden: '81',
      nombreC: 'Honduras',
      nombreOficial: 'La República de Honduras',
      sigla2: 'HN',
      sigla3: 'HND',
      codigo: '340'
    },
    {
      imageURL: '../assets/images/hu.png',
      cancion: 'http://www.nationalanthems.info/hu.mp3',
      orden: '82',
      nombreC: 'Hungría',
      nombreOficial: 'Hungría',
      sigla2: 'HU',
      sigla3: 'HUN',
      codigo: '348'
    },
    {
      imageURL: '../assets/images/in.png',
      cancion: 'http://www.nationalanthems.info/be.mp3',
      orden: '83',
      nombreC: 'India',
      nombreOficial: 'La República de la India',
      sigla2: 'IN',
      sigla3: 'IND',
      codigo: '356'
    },
    {
      imageURL: '../assets/images/id.png',
      cancion: 'http://www.nationalanthems.info/id.mp3',
      orden: '84',
      nombreC: 'Indonesia',
      nombreOficial: 'La República de Indonesia',
      sigla2: 'ID',
      sigla3: 'IDN',
      codigo: '360'
    },
    {
      imageURL: '../assets/images/ir.png',
      cancion: 'http://www.nationalanthems.info/ir.mp3',
      orden: '85',
      nombreC: 'Irán',
      nombreOficial: 'La República Islámica del Irán',
      sigla2: 'IR',
      sigla3: 'IRN',
      codigo: '364'
    },
    {
      imageURL: '../assets/images/iq.png',
      cancion: 'http://www.nationalanthems.info/iq.mp3',
      orden: '86',
      nombreC: 'Iraq',
      nombreOficial: 'La República del Iraq',
      sigla2: 'IQ',
      sigla3: 'IRQ',
      codigo: '368'
    },
    {
      imageURL: '../assets/images/ie.png',
      cancion: 'http://www.nationalanthems.info/ie.mp3',
      orden: '87',
      nombreC: 'Irlanda',
      nombreOficial: 'Irlanda',
      sigla2: 'IE',
      sigla3: 'IRL',
      codigo: '372'
    },
    {
      imageURL: '../assets/images/is.png',
      cancion: 'http://www.nationalanthems.info/is.mp3',
      orden: '88',
      nombreC: 'Islandia',
      nombreOficial: 'La República de Islandia',
      sigla2: 'IS',
      sigla3: 'ISL',
      codigo: '352'
    },
    {
      imageURL: '../assets/images/ck.png',
      cancion: 'http://www.nationalanthems.info/ck.mp3',
      orden: '89',
      nombreC: 'Islas Cook',
      nombreOficial: 'Las Islas Cook',
      sigla2: 'CK',
      sigla3: 'COK',
      codigo: '184'
    },
    {
      imageURL: '../assets/images/fo.png',
      cancion: 'http://www.nationalanthems.info/fo.mp3',
      orden: '90',
      nombreC: 'Islas Feroe',
      nombreOficial: 'Las Islas Feroe',
      sigla2: 'FO',
      sigla3: 'FRO',
      codigo: '234'
    },
    {
      imageURL: '../assets/images/mh.png',
      cancion: 'http://www.nationalanthems.info/mh.mp3',
      orden: '91',
      nombreC: 'Islas Marshall',
      nombreOficial: 'la República de las Islas Marshall',
      sigla2: 'MH',
      sigla3: 'MHL',
      codigo: '584'
    },
    {
      imageURL: '../assets/images/sb.png',
      cancion: 'http://www.nationalanthems.info/sb.mp3',
      orden: '92',
      nombreC: 'Islas Salomón',
      nombreOficial: 'Las Islas Salomón',
      sigla2: 'SB',
      sigla3: 'SLB',
      codigo: '090'
    },
    {
      imageURL: '../assets/images/il.png',
      cancion: 'http://www.nationalanthems.info/il.mp3',
      orden: '93',
      nombreC: 'Israel',
      nombreOficial: 'El Estado de Israel',
      sigla2: 'IL',
      sigla3: 'ISR',
      codigo: '376'
    },
    {
      imageURL: '../assets/images/it.png',
      cancion: 'http://www.nationalanthems.info/it.mp3',
      orden: '94',
      nombreC: 'Italia',
      nombreOficial: 'La República Italiana',
      sigla2: 'IT',
      sigla3: 'ITA',
      codigo: '380'
    },
    {
      imageURL: '../assets/images/jm.png',
      cancion: 'http://www.nationalanthems.info/jm.mp3',
      orden: '95',
      nombreC: 'Jamaica',
      nombreOficial: 'Jamaica',
      sigla2: 'JM',
      sigla3: 'JAM',
      codigo: '388'
    },
    {
      imageURL: '../assets/images/jp.png',
      cancion: 'http://www.nationalanthems.info/jp.mp3',
      orden: '96',
      nombreC: 'Japón',
      nombreOficial: 'Japón',
      sigla2: 'JP',
      sigla3: 'JPN',
      codigo: '392'
    },
    {
      imageURL: '../assets/images/jo.png',
      cancion: 'http://www.nationalanthems.info/jo.mp3',
      orden: '97',
      nombreC: 'Jordania',
      nombreOficial: 'El Reino Hachemita de Jordania',
      sigla2: 'JO',
      sigla3: 'JOR',
      codigo: '400'
    },
    {
      imageURL: '../assets/images/ke.png',
      cancion: 'http://www.nationalanthems.info/ke.mp3',
      orden: '98',
      nombreC: 'Kenya',
      nombreOficial: 'La República de Kenya',
      sigla2: 'KE',
      sigla3: 'KEN',
      codigo: '404'
    },
    {
      imageURL: '../assets/images/ki.png',
      cancion: 'http://www.nationalanthems.info/ki.mp3',
      orden: '99',
      nombreC: 'Kiribati',
      nombreOficial: 'La República de Kiribati',
      sigla2: 'KI',
      sigla3: 'KIR',
      codigo: '296'
    },
    {
      imageURL: '../assets/images/kw.png',
      cancion: 'http://www.nationalanthems.info/kw.mp3',
      orden: '100',
      nombreC: 'Kuwait',
      nombreOficial: 'El Estado de Kuwait',
      sigla2: 'KW',
      sigla3: 'KWT',
      codigo: '414'
    },
    {
      imageURL: '../assets/images/kz.png',
      cancion: 'http://www.nationalanthems.info/kz.mp3',
      orden: '101',
      nombreC: 'Kazajstán',
      nombreOficial: 'La República de Kazajstán',
      sigla2: 'KZ',
      sigla3: 'KAZ',
      codigo: '398'
    },
    {
      imageURL: '../assets/images/kg.png',
      cancion: 'http://www.nationalanthems.info/kg.mp3',
      orden: '102',
      nombreC: 'Kirguistán',
      nombreOficial: 'La República Kirguisa',
      sigla2: 'KG',
      sigla3: 'KGZ',
      codigo: '417'
    },
    {
      imageURL: '../assets/images/lv.png',
      cancion: 'http://www.nationalanthems.info/lv.mp3',
      orden: '103',
      nombreC: 'Letonia',
      nombreOficial: 'La República de Letonia',
      sigla2: 'LV',
      sigla3: 'LVA',
      codigo: '428'
    },
    {
      imageURL: '../assets/images/ls.png',
      cancion: 'http://www.nationalanthems.info/ls.mp3',
      orden: '104',
      nombreC: 'Lesotho',
      nombreOficial: 'El Reino de Lesotho',
      sigla2: 'LS',
      sigla3: 'LSO',
      codigo: '426'
    },
    {
      imageURL: '../assets/images/lb.png',
      cancion: 'http://www.nationalanthems.info/lb.mp3',
      orden: '105',
      nombreC: 'Líbano',
      nombreOficial: 'La República Libanesa',
      sigla2: 'LB',
      sigla3: 'LBN',
      codigo: '422'
    },
    {
      imageURL: '../assets/images/lr.png',
      cancion: 'http://www.nationalanthems.info/lr.mp3',
      orden: '106',
      nombreC: 'Liberia',
      nombreOficial: 'La República de Liberia',
      sigla2: 'LR',
      sigla3: 'LBR',
      codigo: '430'
    },
    {
      imageURL: '../assets/images/ly.png',
      cancion: 'http://www.nationalanthems.info/ly.mp3',
      orden: '107',
      nombreC: 'Libia',
      nombreOficial: 'El Estado de Libia',
      sigla2: 'LY',
      sigla3: 'LBY',
      codigo: '434'
    },
    {
      imageURL: '../assets/images/lt.png',
      cancion: 'http://www.nationalanthems.info/lt.mp3',
      orden: '108',
      nombreC: 'Lituania',
      nombreOficial: 'La República de Lituania',
      sigla2: 'LT',
      sigla3: 'LTU',
      codigo: '440'
    },
    {
      imageURL: '../assets/images/lu.png',
      cancion: 'http://www.nationalanthems.info/lu%5E.mp3',
      orden: '109',
      nombreC: 'Luxemburgo',
      nombreOficial: 'El Gran Ducado de Luxemburgo',
      sigla2: 'LU',
      sigla3: 'LUX',
      codigo: '442'
    },
    {
      imageURL: '../assets/images/mk.png',
      cancion: 'http://www.nationalanthems.info/mk.mp3',
      orden: '110',
      nombreC: 'Macedonia del Norte',
      nombreOficial: 'La República de Macedonia del Norte',
      sigla2: 'MK',
      sigla3: 'MKD',
      codigo: '807'
    },
    {
      imageURL: '../assets/images/mg.png',
      cancion: 'http://www.nationalanthems.info/mg.mp3',
      orden: '111',
      nombreC: 'Madagascar',
      nombreOficial: 'La República de Madagascar',
      sigla2: 'MG',
      sigla3: 'MDG',
      codigo: '450'
    },
    {
      imageURL: '../assets/images/my.png',
      cancion: 'http://www.nationalanthems.info/my.mp3',
      orden: '112',
      nombreC: 'Malasia',
      nombreOficial: 'Malasia',
      sigla2: 'MY',
      sigla3: 'MYS',
      codigo: '458'
    },
    {
      imageURL: '../assets/images/mw.png',
      cancion: 'http://www.nationalanthems.info/mw.mp3',
      orden: '113',
      nombreC: 'Malawi',
      nombreOficial: 'La República de Malaw',
      sigla2: 'MW',
      sigla3: 'MWI',
      codigo: '454'
    },
    {
      imageURL: '../assets/images/mv.png',
      cancion: 'http://www.nationalanthems.info/mv.mp3',
      orden: '114',
      nombreC: 'Maldivas',
      nombreOficial: 'La República de Maldivas',
      sigla2: 'MV',
      sigla3: 'MDV',
      codigo: '462'
    },
    {
      imageURL: '../assets/images/ml.png',
      cancion: 'http://www.nationalanthems.info/ml.mp3',
      orden: '115',
      nombreC: 'Malí',
      nombreOficial: 'La República de Malí',
      sigla2: 'ML',
      sigla3: 'MLI',
      codigo: '466'
    },
    {
      imageURL: '../assets/images/mt.png',
      cancion: 'http://www.nationalanthems.info/mt.mp3',
      orden: '116',
      nombreC: 'Malta',
      nombreOficial: 'La República de Malta',
      sigla2: 'MT',
      sigla3: 'MLT',
      codigo: '470'
    },
    {
      imageURL: '../assets/images/ma.png',
      cancion: 'http://www.nationalanthems.info/ma.mp3',
      orden: '117',
      nombreC: 'Marruecos',
      nombreOficial: 'El Reino de Marruecos',
      sigla2: 'MA',
      sigla3: 'MAR',
      codigo: '504'
    },
    {
      imageURL: '../assets/images/mu.png',
      cancion: 'http://www.nationalanthems.info/mu.mp3',
      orden: '118',
      nombreC: 'Mauricio',
      nombreOficial: 'La República de Mauricio',
      sigla2: 'MU',
      sigla3: 'MUS',
      codigo: '480'
    },
    {
      imageURL: '../assets/images/mr.png',
      cancion: 'http://www.nationalanthems.info/mr.mp3',
      orden: '119',
      nombreC: 'Mauritania',
      nombreOficial: 'La República Islámica de Mauritania',
      sigla2: 'MR',
      sigla3: 'MRT',
      codigo: '478'
    },
    {
      imageURL: '../assets/images/mx.png',
      cancion: 'http://www.nationalanthems.info/mx.mp3',
      orden: '120',
      nombreC: 'México',
      nombreOficial: 'Los Estados Unidos Mexicanos',
      sigla2: 'MX',
      sigla3: 'MEX',
      codigo: '484'
    },
    {
      imageURL: '../assets/images/fm.png',
      cancion: 'http://www.nationalanthems.info/fm.mp3',
      orden: '121',
      nombreC: 'Micronesia',
      nombreOficial: 'Los Estados Federados de Micronesia',
      sigla2: 'FM',
      sigla3: 'FSM',
      codigo: '583'
    },
    {
      imageURL: '../assets/images/mz.png',
      cancion: 'http://www.nationalanthems.info/mz.mp3',
      orden: '122',
      nombreC: 'Mozambique',
      nombreOficial: 'La República de Mozambique',
      sigla2: 'MZ',
      sigla3: 'MOZ',
      codigo: '508'
    },
    {
      imageURL: '../assets/images/mc.png',
      cancion: 'http://www.nationalanthems.info/mc.mp3',
      orden: '123',
      nombreC: 'Mónaco',
      nombreOficial: 'El Principado de Mónaco',
      sigla2: 'MC',
      sigla3: 'MCO',
      codigo: '492'
    },
    {
      imageURL: '../assets/images/mn.png',
      cancion: 'http://www.nationalanthems.info/mn.mp3',
      orden: '124',
      nombreC: 'Mongolia',
      nombreOficial: 'Mongolia',
      sigla2: 'MN',
      sigla3: 'MNG',
      codigo: '496'
    },
    {
      imageURL: '../assets/images/me.png',
      cancion: 'http://www.nationalanthems.info/me.mp3',
      orden: '125',
      nombreC: 'Montenegro',
      nombreOficial: 'Montenegro',
      sigla2: 'ME',
      sigla3: 'MNE',
      codigo: '499'
    },
    {
      imageURL: '../assets/images/mm.png',
      cancion: 'http://www.nationalanthems.info/mm.mp3',
      orden: '126',
      nombreC: 'Myanmar',
      nombreOficial: 'La República de la Unión de Myanmar',
      sigla2: 'MM',
      sigla3: 'MMR',
      codigo: '104'
    },
    {
      imageURL: '../assets/images/na.png',
      cancion: 'http://www.nationalanthems.info/na.mp3',
      orden: '127',
      nombreC: 'Namibia',
      nombreOficial: 'La República de Namibia',
      sigla2: 'NA',
      sigla3: 'NAM',
      codigo: '516'
    },
    {
      imageURL: '../assets/images/nr.png',
      cancion: 'http://www.nationalanthems.info/nr.mp3',
      orden: '128',
      nombreC: 'Nauru',
      nombreOficial: 'La República de Nauru',
      sigla2: 'NR',
      sigla3: 'NRU',
      codigo: '520'
    },
    {
      imageURL: '../assets/images/np.png',
      cancion: 'http://www.nationalanthems.info/np.mp3',
      orden: '129',
      nombreC: 'Nepal',
      nombreOficial: 'La República Democrática Federal de Nepal',
      sigla2: 'NP',
      sigla3: 'NPL',
      codigo: '524'
    },
    {
      imageURL: '../assets/images/ni.png',
      cancion: 'http://www.nationalanthems.info/ni.mp3',
      orden: '130',
      nombreC: 'Nicaragua',
      nombreOficial: 'La República de Nicaragua',
      sigla2: 'NI',
      sigla3: 'NIC',
      codigo: '558'
    },
    {
      imageURL: '../assets/images/ne.png',
      cancion: 'http://www.nationalanthems.info/ne.mp3',
      orden: '131',
      nombreC: 'Níger',
      nombreOficial: 'La República de Níger',
      sigla2: 'NE',
      sigla3: 'NER',
      codigo: '562'
    },
    {
      imageURL: '../assets/images/ng.png',
      cancion: 'http://www.nationalanthems.info/ng.mp3',
      orden: '132',
      nombreC: 'Nigeria',
      nombreOficial: 'La República Federal de Nigeria',
      sigla2: 'NG',
      sigla3: 'NGA',
      codigo: '566'
    },
    {
      imageURL: '../assets/images/nu.png',
      cancion: 'http://www.nationalanthems.info/nu.mp3',
      orden: '133',
      nombreC: 'Niue',
      nombreOficial: 'Niue',
      sigla2: 'NU',
      sigla3: 'NIU',
      codigo: '570'
    },
    {
      imageURL: '../assets/images/no.png',
      cancion: 'http://www.nationalanthems.info/no.mp3',
      orden: '134',
      nombreC: 'Noruega',
      nombreOficial: 'El Reino de Noruega',
      sigla2: 'NO',
      sigla3: 'NOR',
      codigo: '578'
    },
    {
      imageURL: '../assets/images/nz.png',
      cancion: 'http://www.nationalanthems.info/ir.mp3',
      orden: '135',
      nombreC: 'Nueva Zelandia',
      nombreOficial: 'Nueva Zelandia',
      sigla2: 'NZ',
      sigla3: 'NZL',
      codigo: '554'
    },
    {
      imageURL: '../assets/images/om.png',
      cancion: 'http://www.nationalanthems.info/om.mp3',
      orden: '136',
      nombreC: 'Omán',
      nombreOficial: 'La Sultanía de Omán',
      sigla2: 'OM',
      sigla3: 'OMN',
      codigo: '512'
    },
    {
      imageURL: '../assets/images/pw.png',
      cancion: 'http://www.nationalanthems.info/pw.mp3',
      orden: '137',
      nombreC: 'Palau',
      nombreOficial: 'La República de Palau',
      sigla2: 'PW',
      sigla3: 'PLW',
      codigo: '585'
    },
    {
      imageURL: '../assets/images/pa.png',
      cancion: 'http://www.nationalanthems.info/pa.mp3',
      orden: '138',
      nombreC: 'Panamá',
      nombreOficial: 'La República de Panamá',
      sigla2: 'PA',
      sigla3: 'PAN',
      codigo: '591'
    },
    {
      imageURL: '../assets/images/pg.png',
      cancion: 'http://www.nationalanthems.info/pg.mp3',
      orden: '139',
      nombreC: 'Papua Nueva Guinea',
      nombreOficial: 'Estado Independiente de Papua Nueva Guinea',
      sigla2: 'PG',
      sigla3: 'PNG',
      codigo: '598'
    },
    {
      imageURL: '../assets/images/pk.png',
      cancion: 'http://www.nationalanthems.info/pk.mp3',
      orden: '140',
      nombreC: 'Pakistán',
      nombreOficial: 'La República Islámica del Pakistán',
      sigla2: 'PK',
      sigla3: 'PAK',
      codigo: '586'
    },
    {
      imageURL: '../assets/images/py.png',
      cancion: 'http://www.nationalanthems.info/py.mp3',
      orden: '141',
      nombreC: 'Paraguay',
      nombreOficial: 'La República del Paraguay',
      sigla2: 'PY',
      sigla3: 'PRY',
      codigo: '600'
    },
    {
      imageURL: '../assets/images/pe.png',
      cancion: 'http://www.nationalanthems.info/pe.mp3',
      orden: '142',
      nombreC: 'Perú',
      nombreOficial: 'La República del Perú',
      sigla2: 'PE',
      sigla3: 'PER',
      codigo: '604'
    },
    {
      imageURL: '../assets/images/pl.png',
      cancion: 'http://www.nationalanthems.info/pl.mp3',
      orden: '143',
      nombreC: 'Polonia',
      nombreOficial: 'La República de Polonia',
      sigla2: 'PL',
      sigla3: 'POL',
      codigo: '616'
    },
    {
      imageURL: '../assets/images/pr.png',
      cancion: 'http://www.nationalanthems.info/pr.mp3',
      orden: '144',
      nombreC: 'Puerto Rico',
      nombreOficial: 'Estado Libre Asociado de Puerto Rico',
      sigla2: 'PR',
      sigla3: 'PRI',
      codigo: '630'
    },
    {
      imageURL: '../assets/images/pt.png',
      cancion: 'http://www.nationalanthems.info/pt.mp3',
      orden: '145',
      nombreC: 'Portugal',
      nombreOficial: 'La República Portuguesa',
      sigla2: 'PT',
      sigla3: 'PRT',
      codigo: '620'
    },
    {
      imageURL: '../assets/images/qa.png', 
      cancion: 'http://www.nationalanthems.info/qa.mp3',
      orden: '146',
      nombreC: 'Qatar',
      nombreOficial: 'El Estado de Qatar',
      sigla2: 'QA',
      sigla3: 'QAT',
      codigo: '634'
    },
    {
      imageURL: '../assets/images/cf.png',
      cancion: 'http://www.nationalanthems.info/cf.mp3',
      orden: '147',
      nombreC: 'República Centroafricana',
      nombreOficial: 'La República Centroafricana',
      sigla2: 'CF',
      sigla3: 'CAF',
      codigo: '140'
    },
    {
      imageURL: '../assets/images/cd.png',
      cancion: 'http://www.nationalanthems.info/cd.mp3',
      orden: '148',
      nombreC: 'República Democrática del Congo',
      nombreOficial: 'La República Democrática del Congo',
      sigla2: 'CD',
      sigla3: 'COD',
      codigo: '180'
    },
    {
      imageURL: '../assets/images/do.png',
      cancion: 'http://www.nationalanthems.info/do.mp3',
      orden: '149',
      nombreC: 'República Dominicana',
      nombreOficial: 'La República Dominicana',
      sigla2: 'DO',
      sigla3: 'DOM',
      codigo: '214'
    },
    {
      imageURL: '../assets/images/ro.png',
      cancion: 'http://www.nationalanthems.info/ro.mp3',
      orden: '150',
      nombreC: 'Rumania',
      nombreOficial: 'Rumania',
      sigla2: 'RO',
      sigla3: 'ROU',
      codigo: '642'
    },
    {
      imageURL: '../assets/images/rw.png',
      cancion: 'http://www.nationalanthems.info/rw.mp3',
      orden: '151',
      nombreC: 'Rwanda',
      nombreOficial: 'La República de Rwanda',
      sigla2: 'RW',
      sigla3: 'RWA',
      codigo: '646'
    },
    {
      imageURL: '../assets/images/ru.png',
      cancion: 'http://www.nationalanthems.info/ru.mp3',
      orden: '152',
      nombreC: 'Rusia',
      nombreOficial: 'Federación Rusa',
      sigla2: 'RU',
      sigla3: 'RUS',
      codigo: '643'
    },
    {
      imageURL: '../assets/images/vc.png',
      cancion: 'http://www.nationalanthems.info/vc.mp3',
      orden: '153',
      nombreC: 'San Vicente y las Granadinas',
      nombreOficial: 'San Vicente y las Granadinas',
      sigla2: 'VC',
      sigla3: 'VCT',
      codigo: '670'
    },
    {
      imageURL: '../assets/images/ws.png',
      cancion: 'http://www.nationalanthems.info/ws.mp3',
      orden: '154',
      nombreC: 'Samoa',
      nombreOficial: 'El Estado Independiente de Samoa',
      sigla2: 'WS',
      sigla3: 'WSM',
      codigo: '882'
    },
    {
      imageURL: '../assets/images/sm.png',
      cancion: 'http://www.nationalanthems.info/sm.mp3',
      orden: '155',
      nombreC: 'San Marino',
      nombreOficial: 'La República de San Marino',
      sigla2: 'SM',
      sigla3: 'SMR',
      codigo: '674'
    },
    {
      imageURL: '../assets/images/lc.png',
      cancion: 'http://www.nationalanthems.info/lc.mp3',
      orden: '156',
      nombreC: 'Santa Lucía',
      nombreOficial: 'Santa Lucía',
      sigla2: 'LC',
      sigla3: 'LCA',
      codigo: '662'
    },
    {
      imageURL: '../assets/images/kn.png',
      cancion: 'http://www.nationalanthems.info/kn.mp3',
      orden: '157',
      nombreC: 'Saint Kitts y Nevis',
      nombreOficial: 'Saint Kitts y Nevis',
      sigla2: 'KN',
      sigla3: 'KNA',
      codigo: '659'
    },
    {
      imageURL: '../assets/images/sc.png',
      cancion: 'http://www.nationalanthems.info/sc.mp3',
      orden: '158',
      nombreC: 'Seychelles',
      nombreOficial: 'La República de Seychelles',
      sigla2: 'SC',
      sigla3: 'SYC',
      codigo: '690'
    },
    {
      imageURL: '../assets/images/sg.png',
      cancion: 'http://www.nationalanthems.info/sg.mp3',
      orden: '159',
      nombreC: 'Singapur',
      nombreOficial: 'La República deSingapur',
      sigla2: 'SG',
      sigla3: 'SGP',
      codigo: '702'
    },
    {
      imageURL: '../assets/images/st.png',
      cancion: 'http://www.nationalanthems.info/st.mp3',
      orden: '160',
      nombreC: 'Santo Tomé y Príncipe',
      nombreOficial: 'La República Democrática de Santo Tomé y Príncipe',
      sigla2: 'ST',
      sigla3: 'STP',
      codigo: '678'
    },
    {
      imageURL: '../assets/images/sn.png',
      cancion: 'http://www.nationalanthems.info/sn.mp3',
      orden: '161',
      nombreC: 'Senegal',
      nombreOficial: 'La República del Senegal',
      sigla2: 'SN',
      sigla3: 'SEN',
      codigo: '686'
    },
    {
      imageURL: '../assets/images/sl.png',
      cancion: 'http://www.nationalanthems.info/sl.mp3',
      orden: '162',
      nombreC: 'Sierra Leona',
      nombreOficial: 'La República de Sierra Leona',
      sigla2: 'SL',
      sigla3: 'SLE',
      codigo: '694'
    },
    {
      imageURL: '../assets/images/rs.png',
      cancion: 'http://www.nationalanthems.info/rs.mp3',
      orden: '163',
      nombreC: 'Serbia',
      nombreOficial: 'La República de Serbia',
      sigla2: 'RS',
      sigla3: 'SRB',
      codigo: '688'
    },
    {
      imageURL: '../assets/images/mk.png',
      cancion: 'http://www.nationalanthems.info/sy.mp3',
      orden: '164',
      nombreC: 'Síria',
      nombreOficial: 'República Árabe Siria',
      sigla2: 'SY',
      sigla3: 'SYR',
      codigo: '760'
    },
    {
      imageURL: '../assets/images/so.png',
      cancion: 'http://www.nationalanthems.info/so.mp3',
      orden: '165',
      nombreC: 'Somalia',
      nombreOficial: 'La República Federal de Somalia',
      sigla2: 'SO',
      sigla3: 'SOM',
      codigo: '706'
    },
    {
      imageURL: '../assets/images/lk.png',
      cancion: 'http://www.nationalanthems.info/lk.mp3',
      orden: '166',
      nombreC: 'Sri Lanka',
      nombreOficial: 'La República Socialista Democrática de Sri Lanka',
      sigla2: 'LK',
      sigla3: 'LKA',
      codigo: '144'
    },
    {
      imageURL: '../assets/images/sd.png',
      cancion: 'http://www.nationalanthems.info/sd.mp3',
      orden: '167',
      nombreC: 'Sudán',
      nombreOficial: 'La República del Sudán',
      sigla2: 'SD',
      sigla3: 'SDN',
      codigo: '736'
    },
    {
      imageURL: '../assets/images/se.png',
      cancion: 'http://www.nationalanthems.info/se.mp3',
      orden: '168',
      nombreC: 'Suecia',
      nombreOficial: 'El Reino de Suecia',
      sigla2: 'SE',
      sigla3: 'SWE',
      codigo: '752'
    },
    {
      imageURL: '../assets/images/ch.png',
      cancion: 'http://www.nationalanthems.info/ch.mp3',
      orden: '169',
      nombreC: 'Suiza',
      nombreOficial: 'La Confederación Suiza',
      sigla2: 'CH',
      sigla3: 'CHE',
      codigo: '756'
    },
    {
      imageURL: '../assets/images/sr.png',
      cancion: 'http://www.nationalanthems.info/sr.mp3',
      orden: '170',
      nombreC: 'Suriname',
      nombreOficial: 'La República de Suriname',
      sigla2: 'SR',
      sigla3: 'SUR',
      codigo: '740'
    },
    {
      imageURL: '../assets/images/za.png',
      cancion: 'http://www.nationalanthems.info/za.mp3',
      orden: '171',
      nombreC: 'Sudáfrica',
      nombreOficial: 'La República de Sudáfrica',
      sigla2: 'ZA',
      sigla3: 'ZAF',
      codigo: '710'
    },
    {
      imageURL: '../assets/images/ss.png',
      cancion: 'http://www.nationalanthems.info/ss.mp3',
      orden: '172',
      nombreC: 'Sudán del Sur',
      nombreOficial: 'La República de Sudán del Sur',
      sigla2: 'SS',
      sigla3: 'SSD',
      codigo: '728'
    },
    {
      imageURL: '../assets/images/tj.png',
      cancion: 'http://www.nationalanthems.info/tj.mp3',
      orden: '173',
      nombreC: 'Tayikistán',
      nombreOficial: 'La República de Tayikistán',
      sigla2: 'TJ',
      sigla3: 'TJK',
      codigo: '762'
    },
    {
      imageURL: '../assets/images/th.png',
      cancion: 'http://www.nationalanthems.info/th.mp3',
      orden: '174',
      nombreC: 'Tailandia',
      nombreOficial: 'El Reino de Tailandia',
      sigla2: 'TH',
      sigla3: 'THA',
      codigo: '764'
    },
    {
      imageURL: '../assets/images/tw.png',
      cancion: 'http://www.nationalanthems.info/tw.mp3',
      orden: '175',
      nombreC: 'Taiwán',
      nombreOficial: 'La República de China',
      sigla2: 'TW',
      sigla3: 'TWN',
      codigo: '158'
    },
    {
      imageURL: '../assets/images/tl.png',
      cancion: 'http://www.nationalanthems.info/tl.mp3',
      orden: '176',
      nombreC: 'Timor-Leste',
      nombreOficial: 'La República Democrática de Timor-Leste',
      sigla2: 'TL',
      sigla3: 'TLS',
      codigo: '626'
    },
    {
      imageURL: '../assets/images/tg.png',
      cancion: 'http://www.nationalanthems.info/tg.mp3',
      orden: '177',
      nombreC: 'Togo',
      nombreOficial: 'La República Togolesa',
      sigla2: 'TG',
      sigla3: 'TGO',
      codigo: '768'
    },
    {
      imageURL: '../assets/images/tk.png',
      cancion: 'http://www.nationalanthems.info/tk.mp3',
      orden: '178',
      nombreC: 'Tokelau',
      nombreOficial: 'Tokelau',
      sigla2: 'TK',
      sigla3: 'TKL',
      codigo: '772'
    },
    {
      imageURL: '../assets/images/to.png',
      cancion: 'http://www.nationalanthems.info/to.mp3',
      orden: '179',
      nombreC: 'Tonga',
      nombreOficial: 'El Reino de Tonga',
      sigla2: 'TO',
      sigla3: 'TON',
      codigo: '776'
    },
    {
      imageURL: '../assets/images/tt.png',
      cancion: 'http://www.nationalanthems.info/tt.mp3',
      orden: '180',
      nombreC: 'Trinidad y Tabago',
      nombreOficial: 'La República de Trinidad y Tabago',
      sigla2: 'TT',
      sigla3: 'TTO',
      codigo: '780'
    },
    {
      imageURL: '../assets/images/tn.png',
      cancion: 'http://www.nationalanthems.info/tn.mp3',
      orden: '181',
      nombreC: 'Túnez',
      nombreOficial: 'La República de Túnez',
      sigla2: 'TN',
      sigla3: 'TUN',
      codigo: '788'
    },
    {
      imageURL: '../assets/images/tm.png',
      cancion: 'http://www.nationalanthems.info/tm.mp3',
      orden: '182',
      nombreC: 'Turkmenistán',
      nombreOficial: 'Turkmenistán',
      sigla2: 'TM',
      sigla3: 'TKM',
      codigo: '795'
    },
    {
      imageURL: '../assets/images/tr.png',
      cancion: 'http://www.nationalanthems.info/tr.mp3',
      orden: '183',
      nombreC: 'Turquía',
      nombreOficial: 'La República de Turquía',
      sigla2: 'TR',
      sigla3: 'TUR',
      codigo: '792'
    },
    {
      imageURL: '../assets/images/tv.png',
      cancion: 'http://www.nationalanthems.info/tv.mp3',
      orden: '184',
      nombreC: 'Tuvalu',
      nombreOficial: 'Tuvalu',
      sigla2: 'TV',
      sigla3: 'TUV',
      codigo: '798'
    },
    {
      imageURL: '../assets/images/ua.png',
      cancion: 'http://www.nationalanthems.info/ua.mp3',
      orden: '185',
      nombreC: 'Ucrania',
      nombreOficial: 'Ucrania',
      sigla2: 'UA',
      sigla3: 'UKR',
      codigo: '804'
    },
    {
      imageURL: '../assets/images/ug.png',
      cancion: 'http://www.nationalanthems.info/ug.mp3',
      orden: '186',
      nombreC: 'Uganda',
      nombreOficial: 'La República de Uganda',
      sigla2: 'UG',
      sigla3: 'UGA',
      codigo: '800'
    },
    {
      imageURL: '../assets/images/uy.png',
      cancion: 'http://www.nationalanthems.info/uy.mp3',
      orden: '187',
      nombreC: 'Uruguay',
      nombreOficial: 'La República Oriental del Uruguay',
      sigla2: 'UY',
      sigla3: 'URY',
      codigo: '858'
    },
    {
      imageURL: '../assets/images/uz.png',
      cancion: 'http://www.nationalanthems.info/uz.mp3',
      orden: '188',
      nombreC: 'Uzbekistán',
      nombreOficial: 'La República de Uzbekistán',
      sigla2: 'UZ',
      sigla3: 'UZB',
      codigo: '860'
    },
    {
      imageURL: '../assets/images/vu.png',
      cancion: 'http://www.nationalanthems.info/vu.mp3',
      orden: '189',
      nombreC: 'Vanuatu',
      nombreOficial: 'La República de Vanuatu',
      sigla2: 'VU',
      sigla3: 'VUT',
      codigo: '548'
    },
    {
      imageURL: '../assets/images/va.png',
      cancion: 'http://www.nationalanthems.info/va.mp3',
      orden: '190',
      nombreC: 'Vaticano',
      nombreOficial: 'Estado de la Ciudad del Vaticano',
      sigla2: 'VA',
      sigla3: 'VAT',
      codigo: '336'
    },
    {
      imageURL: '../assets/images/ve.png',
      cancion: 'http://www.nationalanthems.info/ve.mp3',
      orden: '191',
      nombreC: 'Venezuela',
      nombreOficial: 'La República de Venezuela',
      sigla2: 'VE',
      sigla3: 'VEN',
      codigo: '862'
    },
    {
      imageURL: '../assets/images/vn.png',
      cancion: 'http://www.nationalanthems.info/vn.mp3',
      orden: '192',
      nome: 'Vietnã',
      nombreC: 'Viet Nam',
      nombreOficial: 'La República Socialista de Viet Nam',
      sigla2: 'VN',
      sigla3: 'VNM',
      codigo: '704'
    },
    {
      imageURL: '../assets/images/ye.png',
      cancion: 'http://www.nationalanthems.info/ye.mp3',
      orden: '193',
      nombreC: 'Yemen',
      nombreOficial: 'La República del Yemen',
      sigla2: 'YE',
      sigla3: 'YEM',
      codigo: '887'
    },
    {
      imageURL: '../assets/images/zm.png',
      cancion: 'http://www.nationalanthems.info/zm.mp3',
      orden: '194',
      nombreC: 'Zambia',
      nombreOficial: 'La República de Zambia',
      sigla2: 'ZM',
      sigla3: 'ZMB',
      codigo: '894'
    },
    {
      imageURL: '../assets/images/zw.png',
      cancion: 'http://www.nationalanthems.info/zw.mp3',
      orden: '195',
      nombreC: 'Zimbabwe',
      nombreOficial: 'La República de Zimbabwe',
      sigla2: 'ZW',
      sigla3: 'ZWE',
      codigo: '716'
    }
  ];
  

  constructor() {
  }

  ngOnInit() {
  }


}
